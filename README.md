# kill-the-newsletter on docker
Simple Dockerfile to launch [kill-the-newsletter](https://github.com/leafac/kill-the-newsletter) in a container

## Launch
For a local test run
```
./ build.sh
docker-compose -f docker-compose-local.yml up
```
and visit http://localhost:4000

A compose/stack file for more productive-ready usage may be found in [docker-compose.yml](docker-compose.yml)

## Customize
### Data volume
kill-the-newsletter keeps its status within a sqlite file in directory /data. This directory should be mounted as a volume in order to achieve persistence.

### Configuration volume
A setup file from [ktn_config/configuration.js](ktn_config/configuration.js) is copied into the container. 
If a special setup is required, this may be overwritten by mounting /config.

### Environment variables
The following environment variables should be overwritten for custom usage
```dockerfile
    URL: https://kill-the-newsletter.myhost.com
    EMAIL: smtp://mail.myhost.com
    MAILTO: mailto:itsme@myhost.com
```

