#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

docker build -t ce72/kill-the-newsletter-on-docker:latest .
