FROM ubuntu:noble

EXPOSE 25 4000
VOLUME [ "/data", "/config" ]

RUN apt-get update \
    && apt-get install -y wget \
    && rm -rf /var/lib/apt/lists/* && apt autoremove

RUN wget -q https://github.com/leafac/kill-the-newsletter/releases/download/v1.0.1/kill-the-newsletter--linux--v1.0.1.tgz \
    && tar -xzf kill-the-newsletter--linux--v1.0.1.tgz \
    && rm kill-the-newsletter--linux--v1.0.1.tgz

COPY ktn_config/configuration.js /config/configuration.js

ENTRYPOINT ["/kill-the-newsletter", "/config/configuration.js"]
