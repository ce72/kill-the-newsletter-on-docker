module.exports = async (require) => {
    const killTheNewsletter = require(".").default;

    const { webApplication, emailApplication } = killTheNewsletter("/data");

    webApplication.set("url", process.env.URL);
    webApplication.set("email", process.env.EMAIL);
    webApplication.set("administrator", process.env.MAILTO);

    webApplication.listen(4000, () => {
        console.log(`Web server started at ${webApplication.get("url")}`);
    });
    emailApplication.listen(25, () => {
        console.log(`Email server started at ${webApplication.get("email")}`);
    });

};